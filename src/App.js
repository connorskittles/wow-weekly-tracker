import './App.css';
import Card from './components/Card.jsx';
import hunter from './static/hunter.jpg';

let background = hunter;

function App() {
  return (
    <div className='App'>
      <header
        className='App-header'
        style={{
          backgroundImage: `url(${background})`,
          backgroundSize: 'cover',
        }}
      >
        <div className='title'>WoW Weekly Tracker</div>
        <div className='cards-area'>
          <Card
            props={{
              title: 'Test',
              description:
                'Test test test Test test test Test test test Test test test Test test test Test test test Test test test Test test test Test test test Test test test Test test test Test test test Test test test Test test test Test test test Test test test Test test test Test test test Test test test Test test test Test test test ',
            }}
          />
          <Card
            props={{
              title: 'Test',
              description:
                'Test test test Test test test Test test test Test test test Test test test Test test test Test test test Test test test Test test test Test test test Test test test Test test test Test test test Test test test Test test test Test test test Test test test Test test test Test test test Test test test Test test test ',
            }}
          />
          <Card
            props={{
              title: 'Test',
              description:
                'Test test test Test test test Test test test Test test test Test test test Test test test Test test test Test test test Test test test Test test test Test test test Test test test Test test test Test test test Test test test Test test test Test test test Test test test Test test test Test test test Test test test ',
            }}
          />
          <Card
            props={{
              title: 'Test',
              description:
                'Test test test Test test test Test test test Test test test Test test test Test test test Test test test Test test test Test test test Test test test Test test test Test test test Test test test Test test test Test test test Test test test Test test test Test test test Test test test Test test test Test test test ',
            }}
          />
          <Card
            props={{
              title: 'Test',
              description:
                'Test test test Test test test Test test test Test test test Test test test Test test test Test test test Test test test Test test test Test test test Test test test Test test test Test test test Test test test Test test test Test test test Test test test Test test test Test test test Test test test Test test test ',
            }}
          />
          <Card
            props={{
              title: 'Test',
              description:
                'Test test test Test test test Test test test Test test test Test test test Test test test Test test test Test test test Test test test Test test test Test test test Test test test Test test test Test test test Test test test Test test test Test test test Test test test Test test test Test test test Test test test ',
            }}
          />
          <Card
            props={{
              title: 'Test',
              description:
                'Test test test Test test test Test test test Test test test Test test test Test test test Test test test Test test test Test test test Test test test Test test test Test test test Test test test Test test test Test test test Test test test Test test test Test test test Test test test Test test test Test test test ',
            }}
          />
          <Card
            props={{
              title: 'Test',
              description:
                'Test test test Test test test Test test test Test test test Test test test Test test test Test test test Test test test Test test test Test test test Test test test Test test test Test test test Test test test Test test test Test test test Test test test Test test test Test test test Test test test Test test test ',
            }}
          />
          <Card
            props={{
              title: 'Test',
              description:
                'Test test test Test test test Test test test Test test test Test test test Test test test Test test test Test test test Test test test Test test test Test test test Test test test Test test test Test test test Test test test Test test test Test test test Test test test Test test test Test test test Test test test ',
            }}
          />
        </div>
      </header>
    </div>
  );
}

export default App;
